import os, zipfile

def backuptoZip(folder):
    folder = os.path.abspath(folder)

    number = 1
    #comprobamos is hay más archivos para actualizar el contador
    while True:
        zipFileName = os.path.basename(folder) + '_' + str(number) + '.zip'
        if not os.path.exists(zipFileName):
            break
        number = number + 1

    #creamos el zip con el nombre comprobado
    backupZip = zipfile.ZipFile(zipFileName, 'w')

#recorremos carpetas para crear el zip
    for foldername, subfolders, filenames in os.walk(folder):
        backupZip.write(foldername)
        for filename in filenames:
            backupZip.write(os.path.join(foldername,filename))
    backupZip.close()
    print('Fin del proceso')
