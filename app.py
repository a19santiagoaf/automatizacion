import requests
import backup

# Función para leer el archivo flow.txt
def procesar_data(d):
    resultado = None
    if d is not None:
        datos = d.split('|')
        operador = datos[0]
        comando1 = datos[1]
        comando2 = datos[2]
        comando3 = datos[3]
        resultado = operador, comando1, comando2, comando3
    return resultado

# Recuperación de datos
if __name__ == '__main__':
    # Archivo de respositorio
    #res = requests.get('https://gitlab.iessanclemente.net/abautis/seminarioautomatizacion/-/raw/main/flow.txt?inline=false')
    res = requests.get('https://gitlab.com/a19santiagoaf/automatizacion/-/raw/main/flow.txt')
    print(res.text)
    # Leemos el archivo flow.txt
    datos = procesar_data(res.text.rstrip('\n'))
    print(datos)

    # Comprobamos si hay que realizar el backup.
    isBackup = datos[1].split(':')[1]
    isAuth = datos[3].split(':')[1]

    # Imprimimos el resultado
    print("backup: ", isBackup)
    # Comprobamos permisos
    if int(isAuth):
        # Comprobamos si se desea realizar el backup
        if int(isBackup) == 1:
            backup.backuptoZip('data')
        else:
            print('La opción de realizar el backup no está activada')
    else:
        print('Wooops! No tienes permisos')
